import React from 'react';
import './Todo.scss';

import AddTodo from './AddTodo';
import { toast } from 'react-toastify';
class ListTodo extends React.Component {
    state = {
        listTodos: [
            { id: 'todo1', title: 'doing homework' },
            { id: 'todo2', title: 'watching video' },
            { id: 'todo3', title: 'fixnign busd' },
        ],
        editingTodo: {},
    };

    addTodo = (todo) => {
        this.setState({
            listTodos: [...this.state.listTodos, todo],
        });
    };

    handleDeleteTodo = (todo) => {
        let currentTodo = this.state.listTodos;
        currentTodo = currentTodo.filter((item) => item.id !== todo.id);
        this.setState({
            listTodos: currentTodo,
        });

        toast.success('delete ok');
    };

    handleEditTodo = (todo) => {
        let { editingTodo, listTodos } = this.state;
        let isEmptyObj = Object.keys(editingTodo).length === 0;

        //save
        if (!isEmptyObj && editingTodo.id === todo.id) {
            let listTodoCopy = [...listTodos];
            //Find index of specific object using findIndex method.
            let objIndex = listTodoCopy.findIndex(
                (item) => item.id === todo.id
            );

            //Log object to Console.
            console.log('Before update: ', listTodoCopy[objIndex]);

            //Update object's name property.
            listTodoCopy[objIndex].title = editingTodo.title;

            this.setState({
                listTodos: listTodoCopy,
                editingTodo: {},
            });
            toast.success('update ok');
            return;
        }

        //edit

        this.setState({
            editingTodo: todo,
        });
    };

    handleOnchangeEditTodo = (e) => {
        let editTodoCopy = { ...this.state.editingTodo };
        editTodoCopy.title = e.target.value;
        this.setState({
            editingTodo: editTodoCopy,
        });
    };

    render() {
        let { listTodos, editingTodo } = this.state;

        let isEmptyObj = Object.keys(editingTodo).length === 0;
        console.log('>>>>>>> check emptyy onbj: ' + isEmptyObj);
        return (
            <>
                <p>Simple todo app & jdj</p>

                <div className="list-todo-container">
                    <AddTodo addTodo={this.addTodo} />
                    <div className="list-todo-content">
                        {listTodos &&
                            listTodos.length > 0 &&
                            listTodos.map((item, index) => {
                                return (
                                    <div className="todo-child" key={item.id}>
                                        {isEmptyObj ? (
                                            <span>
                                                {index + 1} - {item.title}
                                            </span>
                                        ) : (
                                            <>
                                                {editingTodo.id === item.id ? (
                                                    <span>
                                                        {index + 1} -
                                                        <input
                                                            onChange={(e) =>
                                                                this.handleOnchangeEditTodo(
                                                                    e
                                                                )
                                                            }
                                                            value={
                                                                editingTodo.title
                                                            }
                                                        />
                                                    </span>
                                                ) : (
                                                    <span>
                                                        {index + 1} -{' '}
                                                        {item.title}
                                                    </span>
                                                )}
                                            </>
                                        )}

                                        {isEmptyObj &&
                                        editingTodo.id !== item.id ? (
                                            <>
                                                <button
                                                    className="edit"
                                                    onClick={() =>
                                                        this.handleEditTodo(
                                                            item
                                                        )
                                                    }
                                                >
                                                    Edit
                                                </button>
                                            </>
                                        ) : (
                                            <button
                                                className="save"
                                                onClick={() =>
                                                    this.handleEditTodo(item)
                                                }
                                            >
                                                Save
                                            </button>
                                        )}
                                        <button
                                            className="delete"
                                            onClick={() =>
                                                this.handleDeleteTodo(item)
                                            }
                                        >
                                            Delete
                                        </button>
                                    </div>
                                );
                            })}
                    </div>
                </div>
            </>
        );
    }
}
export default ListTodo;
