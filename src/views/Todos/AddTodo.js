import React from 'react';
import { toast } from 'react-toastify';

class AddTodo extends React.Component {
    state = {
        title: '',
    };

    handleOnchangeTitle = (e) => {
        this.setState({
            title: e.target.value,
        });
    };

    handleOnclickAdd = (e) => {
        e.preventDefault();
        if (!this.state.title) {
            alert('missing titke');
            return;
        }
        let todo = {
            id: Math.floor(Math.random() * 1000),
            title: this.state.title,
        };

        this.props.addTodo(todo);
        this.setState({
            title: '',
        });
        toast.success('Added!');
    };
    render() {
        let { title } = this.state;
        return (
            <div className="add-todo">
                <form>
                    <input
                        value={title}
                        type="text"
                        onChange={(e) => this.handleOnchangeTitle(e)}
                    />
                    <button
                        type="button"
                        className="add"
                        onClick={(e) => this.handleOnclickAdd(e)}
                    >
                        Add
                    </button>
                </form>
            </div>
        );
    }
}
export default AddTodo;
