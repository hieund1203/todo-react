import React from 'react';
import ChildComponent from './ChildComponent';
import AddComponent from './AddComponent';
class MyComponent extends React.Component {

    state = {
        
        arrJobs: [
            {id: 'job 1', title: 'developer', salary: '500'},
            {id: 'job 2', title: 'accd', salary: '1000'},
            {id: 'job 3', title: 'test', salary: '1500'},
        ]
    }
    
    addNewJob = (job) => {
        this.setState({
            arrJobs: [...this.state.arrJobs, job]
        })
    }

    deleteJob = (job) => {
        let currentJob = this.state.arrJobs;
        currentJob = currentJob.filter(item => item.id !== job.id)
        this.setState({
            arrJobs: currentJob
        })
    }
    
    render(){
    
        return (
            <>
                <AddComponent
                    addNewJob={this.addNewJob}
                    
                />
                <ChildComponent deleteJob = {this.deleteJob} namez={'child 1'} arrJobs={this.state.arrJobs} />
            </>
        
        );
    }
}
export default MyComponent;