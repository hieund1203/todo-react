import React from 'react';
import './Demo.css'
class ChildComponent extends React.Component {
    state = {
        showJob: true
    }

    showHideContent = (e) => {
        this.setState({
            showJob: !this.state.showJob
        });
    }

    handleDelete = (job) => {
        //alert('clicl');
        this.props.deleteJob(job);
    }
    render(){
    
        let {arrJobs} = this.props;
        let { showJob } = this.state;
        return (
            <>
                <div><button className='btn-show' onClick={(e) => this.showHideContent(e)}>{showJob ? 'Hide' : 'Show' }</button></div>
                {
                    showJob && 
                    <>
                        <div className="job-lists">
                            {
                            arrJobs.map((item, index) => {
                                return (
                                        <div key={item.id}>
                                        {item.title} - {item.salary}   <span onClick={() => this.handleDelete(item)}>x</span>
                                    </div>
                                    )
                                })
                            }
                        </div>
                    </>
                }
                
            </>
            
        )
    }
}


export default ChildComponent;