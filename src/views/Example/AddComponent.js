import React from "react";
import ChildComponent from "./ChildComponent";
class AddComponent extends React.Component {

    state = {
        title: '',
        salary : '',
    }
    handleChangeFname = (e) => {
        this.setState({
            title : e.target.value,
        });
    }

    handleChangeLname = (e) => {
        this.setState({
            salary : e.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        console.log(this.state.title + this.state.salary);
        this.props.addNewJob({
            id: Math.floor(Math.random() * 100),
            title: this.state.title,
            salary: this.state.salary
        });

    }

    
    render() {
        return (
            <div>
                    <p>Form</p>
                    <form method="post">
                        Job title:
                        <input value={this.state.fname} onChange={(e) => this.handleChangeFname(e)} />
                        <br />
                        Salary: <input value={this.state.salary} onChange={(e) => this.handleChangeLname(e)} />
                        <br />
                        <button onClick={(event) => this.handleSubmit(event)}>Submit</button>
                    </form>

                    
                    
                    
                </div>
        )
    }
}

export default AddComponent;